package experiments;

import com.google.gson.*;
import experiments.model.Rozvrh;
import org.json.JSONObject;

import java.io.IOException;

public class StagClient
{
    public static void main(String[] args) throws IOException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        String jsonString = Utils.GetContentFromURL(
                "https://stag-demo.uhk.cz/ws/services/rest2/rozvrhy/getRozvrhByMistnost?semestr=ZS&budova=J&mistnost=J9&outputFormat=JSON"
        );
        Rozvrh rozvrh = gson.fromJson(jsonString, Rozvrh.class);
        JSONObject rozvrh1 = new JSONObject(jsonString);

    }
}
